<script type="text/javascript" src="${rc.contextPath}/js/custome/home.js"></script>
<script>
$(document).ready(function() {
		
})
</script>
<div class="body col-md-8 col-md-offset-2">
    <div class="col-md-6">
        <button class="btn btn-default" type="button" onclick="" id="cmdJournalEntry">振替伝票  入力</button>
    </div>
    <div class="col-md-6">
        <button class="btn btn-default" type="button" onclick="" id="cmdJournalCreate">仕　訳　帳</button>
    </div>
    
    <div class="col-md-6">
        <button class="btn btn-default" type="button" onclick="" id="cmdJournalReviseOrPrint">振替伝票　修正・印刷</button>
    </div>
    <div class="col-md-6">
        <button class="btn btn-default" type="button" onclick="" id="cmdOpeningBalance">期首残高設定</button>
    </div>
    
    <div class="col-md-6">
        <button class="btn btn-default" type="button" onclick="" id="cmdOriginalBook">元  帳</button>
    </div>
    <div class="col-md-6">
        <button class="btn btn-default" type="button" onclick="location.href='${rc.contextPath}/勘定科目メンテ'" id="cmdAccountMaintenance">勘定科目メンテ</button>
    </div>
    
    <div class="col-md-6">
        <button class="btn btn-default" type="button" onclick="" id="cmdTrialBalance">残高試算表</button>
    </div>
    <div class="col-md-6 vis_hidden">
        <button class="btn btn-default" type="button" onclick="" id="cmdCreatingBalance">asdfs</button>
    </div>
    
    <div class="col-md-6">
        <button class="btn btn-default" type="button" onclick="" id="cmdAbstractRegistration">摘要登録</button>
    </div>
    <div class="col-md-6 vis_hidden">
        <button class="btn btn-default" type="button" onclick="" id="cmdMonthlyReport"></button>
    </div>
    
    <div class="col-md-6 vis_hidden">
        <button class="btn btn-default" type="button" onclick="" id="cmdQuarterlyBudget"></button>
    </div>
    <div class="col-md-6 vis_hidden">
        <button class="btn btn-default" type="button" onclick="" id="cmdProjectCode"></button>
    </div>
    
    <div class="col-md-6 vis_hidden">
        
    </div>
    <div class="col-md-6">
        <button class="btn btn-default" type="button" onclick="location.href='${rc.contextPath}/user/logout'" id="cmdEnd">ログアウト</button>
    </div>
</div>
        